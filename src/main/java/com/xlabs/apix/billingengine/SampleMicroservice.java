package com.xlabs.apix.billingengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SampleMicroservice implements CommandLineRunner {

    private static Logger logger = LoggerFactory.getLogger(SampleMicroservice.class);

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SampleMicroservice.class);
        addInitHooks(application);
        application.run(SampleMicroservice.class, args);
    }


	/*protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		SpringApplication application = builder.application();
		addInitHooks(application);
		//return super.configure(builder);
	}*/

    static void addInitHooks(SpringApplication application) {
        logger.info("addInitHooks Called...");
        application.addListeners(
                (ApplicationListener<ApplicationStartingEvent>)
                        event -> logger.info("Application is Starting....."),
                (ApplicationListener<ApplicationStartedEvent>)
                        event -> logger.info("Application SampleMicroservice has started.."),
                (ApplicationListener<ApplicationEnvironmentPreparedEvent>)
                        event -> logger.info("Running java version {}", event.getEnvironment().getProperty("java.runtime.version"))
        );
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//
//		PortalAccessPricingPlan pricingPlan = new PortalAccessPricingPlan("Fintech Pricing Plan", 1, "FULL ACCESS", Arrays.asList("Access to all Financial Institutions", "Access to APIs", "Access to Problem Statements"), 
//				new Date(), new Date(), true, 1, null);
//		pricingPlan.addPrice(new Price(Period.MONTHLY, Currency.SGD, 199.00, 0.0));
//		pricingPlan.addPrice(new Price(Period.ANNUALLY, Currency.SGD, 399.00, 0.0));
//		pricingPlanService.addPricingPlan(pricingPlan);
	}

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate ();
	}


    @PostConstruct
    public void doLog() {
        logger.debug(SampleMicroservice.class.getName() + " started.");
    }


}
