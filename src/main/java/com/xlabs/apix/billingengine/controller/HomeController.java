package com.xlabs.apix.billingengine.controller;


import com.xlabs.apix.billingengine.model.Book;
import com.xlabs.apix.billingengine.model.User;
import com.xlabs.apix.billingengine.model.UserResource;
import com.xlabs.apix.billingengine.service.RegisterUseCaseImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.concurrent.Callable;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@Component
@RestController
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    RegisterUseCaseImpl registerUseCaseImpl;

    @PreDestroy
    public void preConstruct() {
        logger.debug(HomeController.class.getName() + " bean is about to be destroyed");
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug(HomeController.class.getName() + " bean created");


    }

    @GetMapping(value = "/", produces = {APPLICATION_JSON_VALUE})
    public String success() {
        logger.debug("success called");
        logger.debug("added logger statement");
        return "success";
    }

    @GetMapping(value = "/homeXml", produces = {APPLICATION_XML_VALUE})
    public Book test() {
        logger.debug("home called");
        Book book = new Book();
        book.setId(1);
        book.setName("test");
        return book;
    }

    @GetMapping(value = "/homeJson", produces = {APPLICATION_JSON_VALUE})
    public Callable<String> test1() throws InterruptedException {
        return new Callable<String>() {
            @Override
            public String call() throws Exception {
                logger.debug("/homeJson called...");
                Thread.sleep(5000);
                return "foobar";
            }
        };

        /*logger.debug("home called");
        BookJson book = new BookJson();
        book.setId(1);
        book.setName("test");
        return book;*/
    }

    @GetMapping(value = "/getName")
    public String getName(@RequestParam("name") String name) {
        logger.debug("getName called");
        logger.debug("request param: " + name);
        return name + " - " + LocalDate.now().toString();
    }

    @GetMapping(value = "/exception")
    public void throwException () {
        try {
            throw new Exception();
        } catch (Exception ex) {
            logger.error("MyException - ", ex);
            logger.error(ex.getMessage(), ex);
        }
    }

    /*@GetMapping(value = "/exception")
    public void throwException1 () {
        try {
            throw new Exception();
        } catch (Exception ex) {
            logger.error("MyException - ", ex);
            logger.error(ex.getMessage(), ex);
        }
    }*/

    @PostMapping(value = "/forums/{forumId}/register", produces = APPLICATION_JSON_VALUE)
    UserResource register(
            @NotBlank @PathVariable("forumId") Long forumId,
            @Valid @RequestBody UserResource userResource,
            @Valid @RequestParam ("sendWelcomeMail") boolean sendWelcomeMail
    ) {
        logger.debug("In register method: ", forumId, userResource);
        User user = new User(userResource.getName(), userResource.getEmail());
        logger.debug("User: " + user.toString());
        UserResource userResource1 = new UserResource(registerUseCaseImpl.registerUser(user, sendWelcomeMail),
                userResource.getName(), userResource.getEmail());
        logger.debug("UserResource: " + userResource1.toString());
        return userResource1;
    }

    @GetMapping(value = "/error", produces = APPLICATION_JSON_VALUE)
    String error() {
        logger.debug("In error: ");
        return "error";
    }
}
