package com.xlabs.apix.billingengine.controller;


public final class MySingleton {

    private static MySingleton instance;

    private MySingleton() {


    }

    synchronized public static MySingleton getInstance() {
        if(instance == null) {
            synchronized (MySingleton.class) {
                if(instance == null) {
                    System.out.println("Creating new Instance...");
                    instance = new MySingleton();
                }
            }
        }
        return instance;
    }

}
