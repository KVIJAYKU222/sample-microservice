package com.xlabs.apix.billingengine.controller;


import com.xlabs.apix.billingengine.model.OrderRequest;
import com.xlabs.apix.billingengine.model.OrderResponse;
import com.xlabs.apix.billingengine.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class PowerMockApiController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/placeOrder")
    public OrderResponse placeOrder (@RequestBody OrderRequest order) {
        return orderService.checkoutOrder(order);
    }
}
