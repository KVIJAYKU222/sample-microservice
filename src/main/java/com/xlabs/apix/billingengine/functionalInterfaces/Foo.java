package com.xlabs.apix.billingengine.functionalInterfaces;

@FunctionalInterface
public interface Foo {

    String method(String string);

}
