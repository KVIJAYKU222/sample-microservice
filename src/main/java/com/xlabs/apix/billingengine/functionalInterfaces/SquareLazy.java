package com.xlabs.apix.billingengine.functionalInterfaces;

import java.util.function.Supplier;

public interface SquareLazy {

    double squareLazy(Supplier<Double> lazyValue);

    default void squareLazy1(Supplier<Double> supplier) {
        System.out.println("called squareLazy1");
    }
}
