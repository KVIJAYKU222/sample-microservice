package com.xlabs.apix.billingengine.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BookJson {

    private int id;

    private String name;
}
