package com.xlabs.apix.billingengine.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Order {

    private int id;
    private String mobile;
    private int amount;
    private int discount;
    private String email;
    private boolean isValid;
    private String message;

//    public Order(int id, String mobile, int amount, int discount, String email, boolean isValid, String message) {
//        this.id = id;
//        this.mobile = mobile;
//        this.amount = amount;
//        this.discount = discount;
//        this.email = email;
//        this.isValid = isValid;
//        this.message = message;
//    }
}
