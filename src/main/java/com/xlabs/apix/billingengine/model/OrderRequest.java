package com.xlabs.apix.billingengine.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderRequest {

    private Order order;


    public OrderRequest(Order order){
        this.order = order;
    }
}
