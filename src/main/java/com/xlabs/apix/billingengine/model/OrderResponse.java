package com.xlabs.apix.billingengine.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderResponse {

    private Order order;

    public OrderResponse(Order order) {
        this.order=order;
    }
}
