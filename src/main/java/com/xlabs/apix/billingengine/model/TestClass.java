package com.xlabs.apix.billingengine.model;

public class TestClass {




    /*
    * Compress a string -> Write a function to compress the string input using counts of repeated characters. Conditions -

When a char consecutively appears n times, the compressed string should contain char followed by this n - aabcccccaaa -> a2b1c5a3.
- if string is not getting smaller by compression, return the original - aabccdeeaa -> a2b1c2d1e2a2 -> aabccdeeaa.
another example - aaabbbaaa -> a3b3a3
    * */

    public static void main(String[] args) {
        String s = "aabcccccaaa";
        //compressString(s);
        System.out.println(compressString1(s));
        //List<String> list = Arrays.asList("a","a","b","c","c","c","c");
    }

    public static String compressString(String s) {
        int counter = 1;
        char previousElement;
        char currentElement;
        char nextElement;
        String result = "";

        char[] array = s.toCharArray();

        currentElement = array[0];
        nextElement = array[1];
        for (int i = 0; i < array.length - 1; i++) {
            previousElement = array[i - 1];
            currentElement = array[i];
            nextElement = array[i + 1];
            if (currentElement == nextElement) {
                counter++;
                nextElement = currentElement;
                result += currentElement + String.valueOf(counter);
            } else {
                result += nextElement + String.valueOf(counter);
            }
        }

        System.out.println("result: " + result);
        return result;
    }

    public static String compressString1(String s) {
        StringBuffer sb = new StringBuffer();
        int count = 1;
        char prev = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            char curr = s.charAt(i);
            if (prev == curr) {
                count++;
            } else {
                sb.append(prev);
                sb.append(count);
                prev = curr;
                count = 1;
            }
        }
        sb.append(prev);
        sb.append(count);
        if (s.length() < sb.length()) {
            return s;
        } else {
            return sb.toString();
        }
    }
}
