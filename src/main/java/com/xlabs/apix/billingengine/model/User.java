package com.xlabs.apix.billingengine.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class User {

    private int id;
    private String name;
    private String email;

    public User(String name, String email){
        this.name = name;
        this.email = email;
    }

}
