package com.xlabs.apix.billingengine.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserResource {

    private long id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    public UserResource(String name, String email) {
        this.name = name;
        this.email = email;
    }

}
