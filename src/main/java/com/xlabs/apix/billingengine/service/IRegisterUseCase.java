package com.xlabs.apix.billingengine.service;


import com.xlabs.apix.billingengine.model.User;

public interface IRegisterUseCase {

       Long registerUser(User user, boolean sendWelcomeEmail);
}
