package com.xlabs.apix.billingengine.service;


import com.xlabs.apix.billingengine.model.Order;
import com.xlabs.apix.billingengine.model.OrderRequest;
import com.xlabs.apix.billingengine.model.OrderResponse;
import com.xlabs.apix.billingengine.util.NotificationUtil;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    public OrderResponse checkoutOrder (OrderRequest orderRequest) {
        String message = NotificationUtil.sendEmail(orderRequest.getOrder().getEmail());
        Order order = new Order();
        order.setId(orderRequest.getOrder().getId());
        order.setMobile(orderRequest.getOrder().getMobile());
        order.setAmount(orderRequest.getOrder().getAmount());
        order.setDiscount(orderRequest.getOrder().getDiscount());
        order.setEmail(orderRequest.getOrder().getEmail());
        order.setValid(orderRequest.getOrder().isValid());
        order.setMessage(message);
        return new OrderResponse(order);
    }
}
