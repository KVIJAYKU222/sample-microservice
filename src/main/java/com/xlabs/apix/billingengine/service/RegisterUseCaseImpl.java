package com.xlabs.apix.billingengine.service;


import com.xlabs.apix.billingengine.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RegisterUseCaseImpl implements IRegisterUseCase {

    private Logger logger = LoggerFactory.getLogger(RegisterUseCaseImpl.class);

       public Long registerUser(User user, boolean sendWelcomeEmail) {
            logger.debug("In registerUser method: ", user, sendWelcomeEmail);
           return sendWelcomeEmail ? 1001 : new Long(Double.doubleToLongBits(Math.random()));
       }
}
