package com.xlabs.apix.billingengine.util;

import com.google.common.util.concurrent.Uninterruptibles;
import com.xlabs.apix.billingengine.functionalInterfaces.SquareLazy;
import com.xlabs.apix.billingengine.model.Employee;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NotificationUtil implements SquareLazy {

    @Override
    public double squareLazy(Supplier<Double> lazyValue) {
        Uninterruptibles.sleepUninterruptibly(1000, TimeUnit.MILLISECONDS);
        return 9d;
    }

    public static void main(String[] args) {
        NotificationUtil util = new NotificationUtil();

        Map<String, Integer> salaries = new HashMap<>();
        salaries.put("John", 10);
        salaries.put("Freddy", 20);
        salaries.put("Samuel", 30);

        System.out.println(salaries);
        salaries.replaceAll((name, oldValue) -> !name.equals("John") ? oldValue : (oldValue + 10000));
        System.out.println(salaries);

        // Function Functional Interface
        Map<String, Integer> nameMap = new HashMap();
        Integer value = nameMap.computeIfAbsent("Samuep[.kki ", s -> s.length());
        System.out.println(value);

        // Supplier Functional Interface
        Supplier<Double> lazyValue = () -> {
            Uninterruptibles.sleepUninterruptibly(1000, TimeUnit.MILLISECONDS);
            return 9d;
        };
        Double valueSquared = util.squareLazy(lazyValue);
        System.out.println(valueSquared);

        // Consumer Functional Interface
        List<String> names = Arrays.asList("abc", "def", "efff");
        names.forEach(name -> System.out.printf(name));
        System.out.println();

        // Bi Consumer Functional Interfaces
        Map<String, Integer> ages = new HashMap<>();
        ages.put("John", 25);
        ages.put("Freddy", 24);
        ages.put("Samuel", 30);

        ages.forEach((name, age) -> System.out.println("name is " + name + " and age is " + age));

        // Predicate Functional Interface
        List<String> predicateNames = Arrays.asList("Angela", "Aaron", "Bob", "Claire", "David");

        List<String> namesWithA = predicateNames.stream()
                .filter(name -> name.startsWith("A"))
                .collect(Collectors.toList());

        System.out.println(namesWithA);

        // Operator Functional Interface
        List<String> operatorNames = Arrays.asList("bob", "josh", "megan");
        operatorNames.replaceAll(name -> name.toUpperCase());
        operatorNames.replaceAll(String::toUpperCase);
        System.out.println(operatorNames);

        //
        List<Integer> values = Arrays.asList(3, 5, 8, 9, 12);
        int sum = values.stream()
                .reduce(0, (i1, i2) -> i1 + i2);
        System.out.println(sum);

        Thread thread = new Thread(() -> System.out.println("Hello from another thread"));
        System.out.println(thread.getName());

        // max String
        String maxChar = Stream.of("H", "T", "D", "I", "J").max(Comparator.comparing(String::valueOf)).get();

        String maxValue = Optional.ofNullable(Stream.of("H", "T", "D", "I", "J").max(Comparator.comparing(String::valueOf)).get()).map(String::valueOf).get();
        //String maxValue = opt.map(String::valueOf).orElse(opt.get());

        System.out.println("maxValue: " + maxValue);

        String minChar = Stream.of("H", "T", "D", "I", "J")
                .min(Comparator.comparing(String::valueOf))
                .get();
        System.out.println(minChar);

        Integer maxInt = Stream.of(10, 20, 30, 40, 50, 60)
                .max(Comparator.comparing(Integer::valueOf))
                .get();
        System.out.println(maxInt);

        Integer minInt = Stream.of(10, 20, 30, 40, 50, 60)
                .min(Comparator.comparing(Integer::valueOf))
                .get();
        System.out.println(minInt);

        // comparing by object
        List<Employee> employees = new ArrayList();
        employees.add(new Employee(1, "Lokesh", 36));
        employees.add(new Employee(2, "Alex", 46));
        employees.add(new Employee(3, "Brian", 52));

        Comparator<Employee> employeeComparator = Comparator.comparing(Employee::getAge);

        Employee minEmployee = employees.stream().min(employeeComparator).get();
        Employee maxEmployee = employees.stream().max(employeeComparator).get();

        System.out.println(minEmployee.toString());
        System.out.println(maxEmployee.toString());

        // second highest number
        List<Integer> list = Arrays.asList(1, 3, 4, 5, 2, 8, 9, 3, 6, 10, 20, 23, 2, 5);
        long startTime = System.currentTimeMillis();
        System.out.println(System.currentTimeMillis());
        Optional<Integer> value1 = list.stream()
                .sorted(Collections.reverseOrder())
                .limit(2)
                .skip(1)
                .findFirst();
        long endTime = System.currentTimeMillis();
        if (value1.isPresent()) {
            System.out.println(value1.get());
        }
        System.out.println("Time Taken: " + (endTime - startTime));

        // Duplicate List of elements
        List<String> duplicateList = new ArrayList();
        duplicateList.add("Cat");
        duplicateList.add("Dog");
        duplicateList.add("Cat");
        duplicateList.add("cow");
        duplicateList.add("Cow");
        duplicateList.add("Cow");
        duplicateList.add("Goat");
        Map<String, Long> duplicates = duplicateList.stream().collect(Collectors.groupingBy(e -> e.toLowerCase(), Collectors.counting()));
        System.out.println(duplicates);
    }

    public static String sendEmail (String email) {
        // use email Api
        return  "success";
    }
}
