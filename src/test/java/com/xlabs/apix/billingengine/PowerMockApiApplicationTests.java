/*
package com.xlabs.apix.billingengine;

import com.xlabs.apix.billingengine.model.Order;
import com.xlabs.apix.billingengine.model.OrderRequest;
import com.xlabs.apix.billingengine.model.OrderResponse;
import com.xlabs.apix.billingengine.service.OrderService;
import com.xlabs.apix.billingengine.util.NotificationUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "com.xlabs.apix.billingengine.*")
public class PowerMockApiApplicationTests {

    @InjectMocks
    private OrderService orderService;


    public void init() {
        MockitoAnnotations.initMocks(NotificationUtil.class);
    }

    @Test
    public void testStaticMethod() {
        //Given
        Order order = new Order();
        order.setId(1);
        order.setEmail("abc.com");
        order.setMobile("44444");
        order.setAmount(123);
        order.setDiscount(10);
        order.setValid(true);
        order.setMessage("afafafd");

        String emailId = "test@gmail.com";
        when(NotificationUtil.class);
        //When
        when(NotificationUtil.sendEmail(emailId)).thenReturn("success");
        //Then
        OrderRequest request = new OrderRequest(order);

        OrderResponse response = orderService.checkoutOrder(request);

        assertEquals("success", response.getOrder().getMessage());

    }

}
*/
