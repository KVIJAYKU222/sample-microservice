package com.xlabs.apix.billingengine;

import com.xlabs.apix.billingengine.controller.HomeControllerTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({HomeControllerTest.class})
public class SampleMicroserviceTests {
    public static void main(String[] args) {
        JUnitCore.runClasses(new Class[] { SampleMicroserviceTests.class });
    }


}
