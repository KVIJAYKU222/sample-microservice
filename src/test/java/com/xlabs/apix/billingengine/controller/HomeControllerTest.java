package com.xlabs.apix.billingengine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlabs.apix.billingengine.model.User;
import com.xlabs.apix.billingengine.model.UserResource;
import com.xlabs.apix.billingengine.service.RegisterUseCaseImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest (controllers = {HomeController.class})
public class HomeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RegisterUseCaseImpl registerUseCaseImpl;

    @Test
    public void whenValidInput_thenReturn200() throws Exception {

        UserResource user = new UserResource("Zaphod", "zaphod@galaxy.net");

        mockMvc.perform(post("/forums/{forumId}/register", 42L)
                    .param("sendWelcomeMail", "true")
                    .content(objectMapper.writeValueAsString(user))
                    .contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    public void whenNullUserName_thenReturn400() throws Exception {

        UserResource user = new UserResource(null, "zaphod@galaxy.net");

        mockMvc.perform(post("/forums/{forumId}/register", 42L)
                .param("sendWelcomeMail", "true")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void whenNullEmail_thenReturn400() throws Exception {

        UserResource user = new UserResource("Zaphod", null);

        mockMvc.perform(post("/forums/{forumId}/register", 42L)
                .param("sendWelcomeMail", "true")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void whenNullForumId_thenReturn400() throws Exception {

        UserResource user = new UserResource("Zaphod", "zaphod@galaxy.net");

        mockMvc.perform(post("/forums/{forumId}/register", "null")
                .param("sendWelcomeMail", "true")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void whenNullSendWelcomeMail_thenReturn400() throws Exception {

        UserResource user = new UserResource("Zaphod", "zaphod@galaxy.net");

        mockMvc.perform(post("/forums/{forumId}/register", 42L)
                .param("sendWelcomeMail", "")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void whenValidInput_ThenMapsToBusinessModel() throws Exception {

        UserResource user = new UserResource("Zaphod", "zaphod@galaxy.net");

        mockMvc.perform(post("/forums/{forumId}/register", 42L)
                .param("sendWelcomeMail", "true")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isOk());

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(registerUseCaseImpl, times(1)).registerUser(userCaptor.capture(), eq(true));
        assertThat(userCaptor.getValue().getName()).isEqualTo("Zaphod");
        assertThat(userCaptor.getValue().getEmail()).isEqualTo("zaphod@galaxy.net");

    }

    @Test
    public void whenValidInput_thenReturnsUserResponse() throws Exception {

        UserResource user = new UserResource("Zaphod", "zaphod@galaxy.net");
        System.out.println("user: " + objectMapper.writeValueAsString(user));

        MvcResult mvcResult = mockMvc.perform(post("/forums/{forumId}/register", 42L)
                .param("sendWelcomeMail", "true")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("mvcResult: " + mvcResult);

        UserResource expectedUserResourceBody = new UserResource("Zaphod", "zaphod@galaxy.net");

        System.out.println("expectedUserResourceBody: " + objectMapper.writeValueAsString(expectedUserResourceBody));

        String actualResponseBody = mvcResult.getResponse().getContentAsString();

        System.out.println("actualResponseBody: " + actualResponseBody);

        assertThat(objectMapper.writeValueAsString(expectedUserResourceBody)).isEqualToIgnoringWhitespace(actualResponseBody);

    }

}
